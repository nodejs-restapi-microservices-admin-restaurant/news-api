# Documentation
* [News API](#news-api)

## News API
### GET /news
Returns all News instance.

Example response:
```json
[
    {
        
        "_id": "5dd29010c322138cf5c8a49e",
        "title": "Title1",
        "text": "Text1",
        "img": "Url img1",
        "__v": 0
    },
    {
        "_id": "5dd2901dc322138cf5c8a49f",
        "title": "Title2",
        "text": "Text2",
        "img": "Url img2",
        "__v": 0
    }
]
```

### POST /news
Adds to News instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|text|`string`|
|img|`string`|

Example request:
```json
{
    "title": "Title2",
    "text": "Text2",
    "img": "Url img2"
}
```
Example response:
```json
{
    "message": "Successfully created!"
}
```

### GET /news/:id
Returns a News instance by id.

Example response:
```json
{
    "_id": "5dd2901dc322138cf5c8a49f",
    "title": "Title2",
    "text": "Text2",
    "img": "Url img2",
    "__v": 0
}
```
### PUT /news/:id
Updates to News instance in a database.

|Param|Type|
|--|--|
|title|`string`|
|text|`string`|
|img|`string`|

Example request:
```json
{
    "title": "Title2",
    "text": "Text2",
    "img": "Url img2"
}
```
Example response:
```json
{
    "message": "Successfully updated!"
}
```
### DELETE /news/:id
Delete a News instance by id.

Example response:
```json
{
    "message": "Successfully deleted!"
}
```
